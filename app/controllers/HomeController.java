package controllers;

import controllers.*;
import play.api.Environment;
import play.mvc.*;
import play.data.*;
import play.db.ebean.Transactional;
import models.users.*;
import models.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import javax.inject.Inject;

import static java.lang.Math.toIntExact;

import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    private FormFactory formFactory;


    private Environment env;


    @Inject
    public HomeController(Environment e, FormFactory f) {
        this.env = e;
        this.formFactory = f;
    }

    public Result index() {
        if (getUserFromSession() != null) {
            return redirect(controllers.routes.HomeController.newsfeed());
        }
        return ok(index.render(getUserFromSession()));
    }

    private User getUserFromSession() {
        return User.getUserById(session().get("email"));
    }


    @Security.Authenticated(Secured.class)
    public Result meals() {
        if (getUserFromSession() == null) {
            return redirect(controllers.routes.LoginController.login());
        }

        List<Meal> mealList = Meal.find.orderBy("DATE DESC").where().eq("PROFILE_PROFILE_ID", getUserFromSession().getP()
                .getProfileId())
                .setMaxRows(getUserFromSession().getP().getNoOfMeal()).findList();
        List<Food> food = Food.findAll();


        return ok(meals.render(getUserFromSession(), mealList, food));
    }

    @Security.Authenticated(Secured.class)
    public Result mealhistory() {
        if (getUserFromSession() == null) {
            return redirect(controllers.routes.LoginController.login());
        }
        List<Meal> mealList = Meal.find.orderBy("DATE").where().eq("PROFILE_PROFILE_ID", getUserFromSession().getP().getProfileId()).findList();
        List<Food> food = Food.findAll();

        return ok(mealhistory.render(getUserFromSession(), mealList, food));
    }


    @Security.Authenticated(Secured.class)
    public Result profile() {
        String bmi = getUserFromSession().getP().calculateBMI();
        return ok(profile.render(getUserFromSession(), bmi));
    }

    @Security.Authenticated(Secured.class)
    public Result editProfile() {
        if (getUserFromSession() == null) {
            return redirect(controllers.routes.LoginController.login());
        }
        Form<Profile> editProfileForm = formFactory.form(Profile.class);
        Profile p = Profile.find.where().eq("U_EMAIL", getUserFromSession().getEmail()).findUnique();
        if (Profile.find.where().eq("U_EMAIL", getUserFromSession().getEmail()).findUnique() != null) {
            editProfileForm = editProfileForm.fill(Profile.find.where().eq("U_EMAIL", getUserFromSession().getEmail()).findUnique());
        }

        return ok(editProfile.render(getUserFromSession(), editProfileForm, p));
    }

    @Security.Authenticated(Secured.class)
    public Result newsfeed() throws IOException {
        if (getUserFromSession() == null) {
            return redirect(controllers.routes.LoginController.login());
        }
        List<Post> postsList = Post.findAll();
        Form<Post> addPostForm = formFactory.form(Post.class);
        Form<Comment> addCommentForm = formFactory.form(Comment.class);
        List<Comment> commentsList = Comment.findAll();
        Post p = new Post();
        List<Profile> profiles = Profile.findAll();

        /* GENERATING A MOTIVATIONAL MESSAGE */
        String message = null;
        Random r = new Random();
        File inFile = new File("/home/wdd/Desktop/seconyearproject/public/quote", "cytaty.txt");
        final Path path = Paths.get(inFile.getPath());
        final long lineCount = Files.lines(path).count();
        try {

            Scanner in = new Scanner(inFile);
            int random = r.nextInt(toIntExact(lineCount)) + 1;
            for (int i = 0; i < random; i++) {
                message = in.nextLine();

            }
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        /* end of generating */

        return ok(newsfeed.render(addPostForm, addCommentForm, postsList, commentsList, getUserFromSession(), p, env, profiles, message));
    }


    @Transactional
    public Result generateDiet() {
        Profile p = Profile.find.where().eq("U_EMAIL", getUserFromSession().getEmail()).findUnique();
        for (int i = 0; i < p.getNoOfMeal(); i++) {
            Meal m = new Meal();
            m.setProfile(p);

            String x = m.getTimeOfDay();

            MealFood mf = new MealFood();
            mf.setM(m);
            mf.setF(m.generateProteinFood(x));

            MealFood mf1 = new MealFood();
            mf1.setM(m);
            mf1.setF(m.generateFatsFood(x));

            MealFood mf2 = new MealFood();
            mf2.setM(m);
            mf2.setF(m.generateCarbsFood(x));

            m.save();

            mf.save();
            mf1.save();
            mf2.save();
        }

        return redirect(controllers.routes.HomeController.meals());

    }


    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    public Result foodsview() {
        List<Food> foodsList = new ArrayList<Food>();
        Form<Food> addFoodForm = formFactory.form(Food.class);

        foodsList = Food.findAll();

        return ok(foodsview.render(addFoodForm, foodsList, getUserFromSession()));
    }


    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    public Result usersview() {
        List<User> usersList = User.findAll();
        return ok(usersview.render(usersList, getUserFromSession()));
    }

    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    @Transactional
    public Result deleteUser(String email) {

        // find user by id and call delete method
        User u = User.find.ref(email);
        Profile p = User.find.ref(email).getP();

        u.delete();

        // Add message to flash session
        flash("success", "User has been deleted");

        // Redirect to usersview page
        return redirect(routes.HomeController.usersview());
    }

    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    @Transactional
    public Result deleteFood(Long id) {

        // find user by id and call delete method
        Food.find.ref(id).delete();
        // Add message to flash session
        flash("success", "Food has been deleted");

        // Redirect to foodsview page
        return redirect(routes.HomeController.foodsview());
    }


    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    public Result addFood() {

        Form<Food> addFoodForm = formFactory.form(Food.class);

        
        return ok(addFood.render(addFoodForm, getUserFromSession()));
    }

    @Security.Authenticated(Secured.class)
    public Result strengthcalculator() {


        return ok(strengthcalculator.render(getUserFromSession()));
    }


    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    public Result addQuote() {

        Form<Quote> addQuoteForm = formFactory.form(Quote.class);

        return ok(addQuote.render(addQuoteForm, getUserFromSession()));
    }


    @Transactional
    public Result addFoodSubmit() {

        Form<Food> newFoodForm = formFactory.form(Food.class).bindFromRequest();

        if (newFoodForm.hasErrors()) {
            return badRequest(addFood.render(newFoodForm, getUserFromSession()));
        }


        Food f = newFoodForm.get();

        if (newFoodForm.get().getMainMacro().equals("Protein")) {
            f = new ProteinFood();

        } else if (newFoodForm.get().getMainMacro().equals("Carbohydrates")) {
            f = new CarbsFood();
        } else {
            f = new FatFood();
        }

        f.setFoodName(newFoodForm.get().getFoodName());
        f.setMainMacro(newFoodForm.get().getMainMacro());
        f.setKcal(newFoodForm.get().getKcal());
        f.setProtein(newFoodForm.get().getProtein());
        f.setCarbs(newFoodForm.get().getCarbs());
        f.setFat(newFoodForm.get().getFat());
        f.setSuitable(newFoodForm.get().getSuitable());


        if (f.getFoodId() == null) {
          
            f.save();
        }
        
        else if (f.getFoodId() != null) {

            f.update();
        }


        flash("success", "Food " + f.getFoodName() + " has been created/ updated");

        return redirect(controllers.routes.HomeController.foodsview());
    }


    @Transactional
    public Result addQuoteSubmit() throws IOException {
        Form<Quote> newForm = formFactory.form(Quote.class).bindFromRequest();

        Quote q = newForm.get();
        System.out.println(q.getQuote());
        q.writeToFile();

        flash("success", "Your quote has been added.");

        return redirect(controllers.routes.HomeController.addQuote());
    }


    @Transactional
    public Result deletePost(Long id) {

        Plike like = Plike.findByPost(id);
        List<Comment> comments = Comment.findByPost(id);

        List<User> users = User.deleteLike(like.getLikeId());

        for (int i = 0; i < users.size(); i++) {
            for (int j = 0; j < users.get(i).getLikes().size(); j++) {
                if (users.get(i).getLikes().get(j).getLikeId() == like.getLikeId()) {
                    users.get(i).getLikes().get(j).removeUser(users.get(i));
                    users.get(i).update();
                }

            }
        }

        like.delete();


        for (int i = 0; i < comments.size(); i++) {

            comments.get(i).delete();
        }


        Post.find.ref(id).delete();


        /* delete image related to the post */
        File f = new File("/home/wdd/Desktop/seconyearproject/public/images/postImages/", id + ".jpg");
        File fthumb = new File("/home/wdd/Desktop/seconyearproject/public/images/postImages/thumbnails/", id + ".jpg");

        f.delete();
        fthumb.delete();

        flash("success", "Post has been deleted");

  
        return redirect(routes.HomeController.newsfeed());
    }


}
