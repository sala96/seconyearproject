package controllers;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;
import play.api.Environment;
import play.mvc.*;
import play.data.*;
import play.db.ebean.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import views.html.*;

import models.users.*;
import models.*;


public class LoginController extends Controller {



    private FormFactory formFactory;

    private Environment env;

    @Inject
    public LoginController(Environment e, FormFactory f) {
        this.env = e;
        this.formFactory = f;
    }

    private User getUserFromSession() {
        return User.getUserById(session().get("email"));
    }


    public Result login() {

        Form<Login> loginForm = formFactory.form(Login.class);

        return ok(login.render(loginForm, User.getUserById(session().get("email"))));
    }

    public Result loginSubmit() {

        Form<Login> loginForm = formFactory.form(Login.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm, User.getUserById(session().get("email"))));
        } else {
            session().clear();
            session("email", loginForm.get().getEmail());
        }

        return redirect(controllers.routes.HomeController.newsfeed());
    }

    public Result logout() {
        session().clear();
        flash("success", "You've  been logged out");
        return redirect(routes.LoginController.login());
    }

    public Result register() {
        Form<User> newUserForm = formFactory.form(User.class);

        return ok(register.render(newUserForm, User.getUserById(session().get("email"))));
    }


    @Transactional
    public Result addUserSubmit() {
        Form<Member> newUserForm = formFactory.form(Member.class).bindFromRequest();
        Member u = new Member();
        try {
            u = newUserForm.get();
        } catch (Exception e) {
            flash("failReg", "Fill in all the fields");
            return redirect(controllers.routes.LoginController.register());
        }

       if(User.findByEmail(u.getEmail()) != null) {
           flash("failReg", "This email is already used,  try again");
           return redirect(controllers.routes.LoginController.register());
       }


        if (u.getEmail() != null) {
            u.save();
            flash("success", "User " + u.getEmail() + " has been registered.");
        }

        session("email", newUserForm.get().getEmail());
        Profile p = new Profile();
        p.setU(getUserFromSession());
        p.save();
        u.setP(p);
        u.update();

        return redirect(controllers.routes.HomeController.editProfile());
    }

    @Transactional
    public Result editProfileForm(Long id) {
        String saveImageMsg;

        Form<Profile> newProfileForm = formFactory.form(Profile.class).bindFromRequest();
        Profile p = new Profile();
        try {
            p = newProfileForm.get();
        } catch (Exception e) {
            flash("profileFail", "Invalid Input, please try again");
            return redirect(controllers.routes.HomeController.editProfile());

        }


        p.calculateDailyKcal();
        p.setProfileId(id);

        Http.MultipartFormData data = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart image = data.getFile("upload");

        saveImageMsg = addProfilePic(p.getProfileId(), image);


        if (p.getProfileId() == null) {
            p.save();
        } else if (p.getProfileId() != null) {

            p.update();
            flash("profileSuccess", "Profile has been saved succesfully");
        }


        if (newProfileForm.hasErrors()) {
            return redirect(controllers.routes.HomeController.editProfile());
        }

        return redirect(controllers.routes.HomeController.newsfeed());
    }

    public String addProfilePic(Long id, Http.MultipartFormData.FilePart<File> uploaded) {
        if (uploaded != null) {
            String mimeType = uploaded.getContentType();
            if (mimeType.startsWith("image/")) {
                File file = uploaded.getFile();
                ConvertCmd cmd = new ConvertCmd();
                IMOperation op = new IMOperation();

                op.addImage(file.getAbsolutePath());
                op.resize(320, 260);

                op.addImage("public/images/ProfileImages/" + id + ".jpg");

                IMOperation thumb = new IMOperation();
                thumb.addImage(file.getAbsolutePath());
                thumb.thumbnail(60);
                thumb.addImage("public/images/ProfileImages/thumbnails/" + id + ".jpg");

                try {
                    cmd.run(op);
                    cmd.run(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return "and iamge saved";

            }
        }
        return "no file";
    }

    @Transactional
    public Result setAdmin(String email) {
        User u = User.find.ref(email);
        u.setRole("admin");
        u.update();
        return redirect(controllers.routes.HomeController.usersview());
    }





}
    
