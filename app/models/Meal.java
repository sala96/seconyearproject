package models;

import java.util.*;
import javax.persistence.*;
import java.util.Random;

import models.users.*;
import com.avaje.ebean.Model;


@Entity
public class Meal extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long mealId;

    @ManyToOne
    private Profile profile;

/*    // many to many mapping
    @ManyToMany(cascade = CascadeType.REMOVE)
    private List<Food> foods = new ArrayList<>();*/

  @OneToMany(mappedBy = "m")
    private List<MealFood> foods = new ArrayList<>();


    private Date date;

    private double totalKcal;

    private double totalCarbs;
    private double totalProtein;
    private double totalFat;

    private int mealNo;


    public Meal() {
        date = new Date();
    }

    public Meal(Profile uIn) {
        profile = uIn;

    }


    public static Finder<Long, Meal> find = new Finder<Long, Meal>(Meal.class);


    public static List<Meal> findAll() {
        return Meal.find.all();
    }

    public List<Food> findFoodsByMeal(Meal m, Food f) {
        return Food.find.where()
                    .eq("M_MEAL_ID", m.getMealId())
                    .eq("F_FOOD_ID", f.getFoodId()).findList();
    }





    public long getMealId() {
        return mealId;
    }

    public void setMealId(long mealId) {
        this.mealId = mealId;
    }

    public Profile getProfile() {
        return profile;
    }

    public int getMealNo() {
        return mealNo;
    }

    public void setMealNo(int mealNo) {
        this.mealNo = mealNo;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

/*    public List<MealFood> getFoods() {
        return foods;
    }

    public void setFoods(List<MealFood> foods) {
        this.foods = foods;
    }*/

    public List<MealFood> getFoods() {
        return foods;
    }

    public void setFoods(List<MealFood> foods) {
        this.foods = foods;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalKcal() {

        return Math.ceil(totalKcal);

    }

    public void setTotalKcal(double totalKcal) {
        this.totalKcal = totalKcal;
    }

    public double getTotalCarbs() {
        return Math.ceil(totalCarbs);
    }

    public void setTotalCarbs(double totalCarbs) {
        this.totalCarbs = totalCarbs;
    }

    public double getTotalProtein() {
        return Math.ceil(totalProtein);
    }

    public void setTotalProtein(double totalProtein) {
        this.totalProtein = totalProtein;
    }

    public double getTotalFat() {
        return Math.ceil(totalFat);
    }

    public void setTotalFat(double totalFat) {
        this.totalFat = totalFat;
    }


    public boolean hasMeal() {
        if (find.where().eq("PROFILE_PROFILE_ID", profile.getProfileId()).findRowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public int rowCount() {
        int fajnynumberek = 100 * profile.getNoOfMeal();
        int count = (find.where().eq("PROFILE_PROFILE_ID", profile.getProfileId()).findRowCount()) % profile.getNoOfMeal();


        if(count == 0){
            mealNo = ((fajnynumberek - 0) % profile.getNoOfMeal())+1;
        }
        else if (count == 1){
            mealNo = ((fajnynumberek - 4) % profile.getNoOfMeal()) +1 ;
        }
        else if (count == 2){
            mealNo = ((fajnynumberek - 3) % profile.getNoOfMeal()) + 1;
        }
        else if (count == 3){
            mealNo = ((fajnynumberek - 2) % profile.getNoOfMeal()) + 1;
        }
        else if (count == 4){
            mealNo = ((fajnynumberek - 1) % profile.getNoOfMeal()) + 1;
        }


        // * //

       // mealNo = (fajnynumberek - count) % profile.getNoOfMeal()+1;

        return count;
    }

    public Food generateProteinFood(String suitable) {

        Food f = new Food();
        f = f.generateProtein(suitable);
        double proteinRequiredPerMeal = (profile.getWeight() * 2.2) / profile.getNoOfMeal();
        double proteinAmount = (proteinRequiredPerMeal * 100) / f.getProtein();       // gram amount of food rich in protein
        double totalProteinCalories = (f.getKcal() * proteinAmount) / 100;
        totalFat += (f.getFat() * proteinAmount) / 100;
        totalProtein += proteinAmount;
        totalKcal += totalProteinCalories;
        return f;

    }


    public Food generateFatsFood(String suitable) {
        Food f = new Food();

        f = f.generateFats(suitable);
        double fatsRequiredPerMeal = (((profile.getDaily_kcal() / 4) / 9) / profile.getNoOfMeal() - totalFat); // grams of fats per meal
        double fatsAmount = (fatsRequiredPerMeal * 100) / f.getFat();
        double totalFatsCalories = (f.getKcal() * fatsAmount) / 100;
        totalFat += fatsAmount;

        totalKcal += totalFatsCalories;
        return f;

    }


    public Food generateCarbsFood(String suitable) {
        double leftcalories = (profile.getDaily_kcal() / profile.getNoOfMeal()) - totalKcal;

        System.out.println("calories left: "+leftcalories);

        Food f = new Food();
        f = f.generateCarbs(suitable);
        double carbsAmount = ((leftcalories / 4) * 100)  / f.getCarbs(); // grams of carbs
        double totalCarbsCalories = (f.getKcal() * carbsAmount) / 100;
        totalCarbs += carbsAmount;


        totalKcal += totalCarbsCalories;
        return f;

    }


    public void generateMeal(String name) {

        generateProteinFood(name);
        generateFatsFood(name);
        generateCarbsFood(name);

    }

    public String getTimeOfDay() {
        rowCount();

        if (mealNo == 1) {
            return "morning";
        }
        if (mealNo == profile.getNoOfMeal()) {
            return "night";
        } else {
            return "afternoon";
        }

    }
}