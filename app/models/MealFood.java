package models;

import java.util.*;

import play.data.validation.*;

import javax.persistence.*;

import models.*;
import com.avaje.ebean.Model;


@Entity
public class MealFood extends Model {

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private Date date;

    @ManyToOne
     private Meal m;

    @ManyToOne
    private Food f;

    public static Finder<Long, MealFood> find = new Finder<Long, MealFood>(MealFood.class);

    public MealFood() {
        date = new Date();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Meal getM() {
        return m;
    }

    public void setM(Meal m) {
        this.m = m;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Food getF() {
        return f;
    }

    public void setF(Food f) {
        this.f = f;
    }
}