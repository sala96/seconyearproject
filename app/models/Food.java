package models;


import java.util.*;

import play.data.validation.*;

import javax.persistence.*;

import models.*;
import com.avaje.ebean.Model;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "main")
public class Food extends Model {

    @Id
    private Long foodId;

/*    @ManyToMany(cascade = CascadeType.REMOVE, mappedBy = "foods")
    private List<Meal> meals = new ArrayList<>();*/

    @OneToMany(mappedBy = "f")
    private List<MealFood> meals = new ArrayList<>();


    @Constraints.Required
    private String foodName;

    @Constraints.Required
    private String suitable;

    @Constraints.Required
    private double kcal;

    @Constraints.Required
    private double protein;

    @Constraints.Required
    private double carbs;

    @Constraints.Required
    private double fat;


    @Constraints.Required
    private String mainMacro;




    public Food() {


    }


    public Food(Long foodId, String foodName, String suitable, double kcal, double protein, double carbs, double fat, String mainMacro) {
        this.foodId = foodId;
        this.suitable = suitable;
        this.foodName = foodName;
        this.kcal = kcal;
        this.protein = protein;
        this.carbs = carbs;
        this.fat = fat;
        this.mainMacro = mainMacro;
    }


    //Generic query helper for entity Food with id Long
    public static Model.Finder<Long, Food> find = new Model.Finder<Long, Food>(Food.class);

    // Find all Food in the database
    public static List<Food> findAll() {
        return Food.find.all();
    }

    public static List<Food> findFoodByMeal(Long mealId) {
        return Food.find.where()
                // Only include products from the matching cat ID
                // In this case search the ManyToMany relation
                .eq("meals.mealId", mealId).findList();
    }

    public int rowCount() {
        return (find.where().findRowCount());
    }


    public Food generateProtein(String suitable) {
        Random r = new Random();
        Food f = null;
        while (f == null) {
            int random = r.nextInt(rowCount()) + 1;

            f = find.where().eq("food_id", random).and().eq("main", "protein").and().eq("suitable", suitable).findUnique();

        }

        return f;
    }


    public Food generateFats(String suitable) {

        Random r = new Random();

        Food f = null;
        while (f == null) {
            int random = r.nextInt(rowCount()) + 1;

            f = find.where().eq("food_id", random).and().eq("main", "fat").and().eq("suitable", suitable).findUnique();

        }

        return f;
    }


    public Food generateCarbs(String suitable) {
        Random r = new Random();
        Food f = null;
        while (f == null) {
            int random = r.nextInt(rowCount()) + 1;
            f = find.where().eq("food_id", random).and().eq("main", "carbs").and().eq("suitable", suitable).findUnique();

        }
        return f;
    }

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

/*    public List<MealFood> getMeals() {
        return meals;
    }

    public void setMeals(List<MealFood> meals) {
        this.meals = meals;
    }*/

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getSuitable() {
        return suitable;
    }

    public void setSuitable(String suitable) {
        this.suitable = suitable;
    }

    public double getKcal() {
        return kcal;
    }

    public void setKcal(double kcal) {
        this.kcal = kcal;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public String getMainMacro() {
        return mainMacro;
    }

    public void setMainMacro(String mainMacro) {
        this.mainMacro = mainMacro;
    }

    public List<MealFood> getMeals() {
        return meals;
    }

    public void setMeals(List<MealFood> meals) {
        this.meals = meals;
    }
}
